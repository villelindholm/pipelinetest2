// index.js

import express from 'express'

const server = express()

server.get('/', (request, response) => {
    response.send('hello kultsi')
})

server.get('/version', (request, response) => {
    const version = process.env.VERSION
    response.send('Version:'+version)
})


server.get('/secret', (request, response) => {
    const secret = process.env.SECRET
    response.send(secret)
})

server.listen(3000, () => {
    console.log('API running @ 3000')
})