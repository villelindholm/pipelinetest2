FROM node:20-alpine

WORKDIR /app

COPY ./package*.json .
RUN npm install

COPY ./index.js . 

ARG APP_VERSION=0.0.0
ENV VERSION=${APP_VERSION}

ENV SECRET=SALAISUUS

EXPOSE 3000
CMD [ "node", "./index.js" ]